import datetime
from collections import OrderedDict

import pandas as pd
from models import Optimizations, db, ErrorTable

DATA_BARS_RED = {'red': ["Mispelling", "Url Error"]}


def combine(dictionaries):
    combined_dict = {}
    for dictionary in dictionaries:
        for key, value in dictionary.items():
            combined_dict.setdefault(key, []).append(value)
    for key in combined_dict.keys():
        combined_dict[key] = list(OrderedDict.fromkeys(combined_dict[key]))
    return combined_dict


def split_by(table, date_a, data_of_color, table_key,
             item_type, distinct_key, itter, date_key):
    ret = [0, 0]
    for model in table:
        if isinstance(model, (ErrorTable, Optimizations)):
            model = vars(model)
        model_date = model[date_key]
        if isinstance(model_date, datetime.date):
            model_date = model_date.strftime('%Y-%m-%d')
        if date_a[0].strftime('%Y-%m-%d') <= model_date <= date_a[1]\
                .strftime('%Y-%m-%d') and model[table_key] == distinct_key:
            if model[item_type] in data_of_color['red']:
                ret[0] += int(model[itter])
            else:
                ret[1] += int(model[itter])
    return ret


def data_dict(distinct_dict, table, data_of_color,
              date, type_, table_key, itter, distinct_key, date_key):
    return {key: split_by(table, date, data_of_color,
                          table_key, type_, key, itter, date_key)
            for key in distinct_dict[distinct_key]}


def distinct_values(data, distinct_init, type_):
    distinct = distinct_init
    exec(
        f"""for model in data:
            distinct['Market'].append(model.market)
            distinct['Campaign'].append(model.campaign)
            distinct['Platform'].append(model.platform)
            distinct['Type'].append(model.{type_})
            distinct['Client'].append(model.client)"""
    )
    for key in distinct.keys():
        distinct[key] = list(set(distinct[key]))
    return distinct


def optimization_table(query_results):
    data_ = {
        'Date': [],
        'Market': [],
        'Client': [],
        'Account': [],
        'Compaign': [],
        'Check Type': [],
        'Platform': [],
        'Entity type': [],
        'Score': [],
        'Cost': []
    }
    for opti in query_results:
        data_['Date'].append(opti.date)
        data_['Market'].append(opti.market)
        data_['Client'].append(opti.client)
        data_['Account'].append(opti.account)
        data_['Compaign'].append(opti.campaign)
        data_['Check Type'].append(opti.check_)
        data_['Platform'].append(opti.platform)
        data_['Entity type'].append(opti.entity_type)
        data_['Score'].append(opti.score)
        data_['Cost'].append(opti.cost)
    return pd.DataFrame(data_)


def error_table(query_result):
    data_ = {
        'Date': [],
        'Market': [],
        'Client': [],
        'Account': [],
        'Compaign': [],
        'AdGroup': [],
        'Error Type': [],
        'Platform': [],
        'Level': [],
        'Entity Type': [],
        'Item Detail': [],
        'count': []
    }

    for model in query_result:
        data_['Date'].append(model.date)
        data_['Market'].append(model.market)
        data_['Client'].append(model.client)
        data_['Account'].append(model.account)
        data_['Compaign'].append(model.campaign)
        data_['AdGroup'].append(model.adgroup)
        data_['Error Type'].append(model.item_type)
        data_['Platform'].append(model.platform)
        data_['Level'].append(model.level)
        data_['Entity Type'].append(model.entity_type)
        data_['Item Detail'].append(model.item_details)
        data_['count'].append(model.count)
    return pd.DataFrame(data_)


def section_data(app):
    date_a = datetime.datetime.now()
    date_b = date_a - datetime.timedelta(90)

    data = {'red': ["Mispelling", "Url Error"]}
    with app.server.app_context():
        distinct_errors = {'Market': [], 'Campaign': [],
                           'Platform': [], 'Type': [], 'Client': []}
        distinct_optimizations = {'Client': [], 'Platform': [],
                                  'Type': [], 'Campaign': [], 'Market': []}
        optimizations = list(Optimizations.query
                             .filter(Optimizations.date <= date_a.strftime('%Y-%m-%d'))
                             .filter(Optimizations.date >= date_b.strftime('%Y-%m-%d')))
        errors = list(ErrorTable.query.filter(ErrorTable.date <= date_a.strftime('%Y-%m-%d'))
                                .filter(ErrorTable.date >= date_b.strftime('%Y-%m-%d')))
        distinct_errors = distinct_values(errors, distinct_errors, 'item_type')
        distinct_optimizations = distinct_values(optimizations,
                                                 distinct_optimizations,
                                                 'check_')
        distinct_cli_models = list(set([*distinct_optimizations['Client'],
                                        *distinct_errors['Client']]))
        data['client'] = distinct_cli_models
        data['table_1'] = error_table(errors)
        data['table_2'] = optimization_table(optimizations)
        data['empty_bar_chart'] = {'': [0, 0]}
    return data


def line_basic(date_b, day_format):
    return {
        'AdWords': {'x': [(date_b+datetime.timedelta(n)).strftime('%d-%m-%Y')
                          for n in range(1, day_format+1)],
                    'y': [0]*day_format
                    },
        'Facebook': {
            'x': [(date_b + datetime.timedelta(n)).strftime('%d-%m-%Y') for n
                  in range(1, day_format + 1)],
            'y': [0]*day_format
            },
        'Bing': {
            'x': [(date_b + datetime.timedelta(n)).strftime('%d-%m-%Y') for n
                  in range(1, day_format + 1)],
            'y': [0]*day_format
            }
    }


def optimizations_line_chart(app, selected_client, day_format):

    date_a = datetime.datetime.now()
    date_b = date_a - datetime.timedelta(day_format)
    ret_data = line_basic(date_b, day_format)
    with app.server.app_context():
        if selected_client is not None:
            query = list(Optimizations.query
                         .filter_by(client=selected_client)
                         .filter(Optimizations.date <= date_a)
                         .filter(Optimizations.date > date_b)
                         .order_by(Optimizations.date.desc())
                         )
        else:
            query = list(Optimizations.query
                         .filter(Optimizations.date <= date_a)
                         .filter(Optimizations.date > date_b)
                         .order_by(Optimizations.date.desc())
                         )
        for result in query:
            index = ret_data[result.platform]['x']\
                .index(result.date.strftime('%d-%m-%Y'))
            ret_data[result.platform]['y'][index] += result.score
    for key in ret_data.keys():
        new = [y if y != 0 else None for y in ret_data[key]['y']]
        ret_data[key]['y'] = new
    return ret_data


def error_line_chart(app, selected_client, day_format):
    date_a = datetime.datetime.now()
    date_b = date_a - datetime.timedelta(day_format)
    ret_data = line_basic(date_b, day_format)
    with app.server.app_context():
        if selected_client is not None:
            query = list(ErrorTable.query
                         .filter_by(client=selected_client)
                         .filter(ErrorTable.date <= date_a)
                         .filter(ErrorTable.date > date_b)
                         .order_by(ErrorTable.date.desc())
                         )
        else:
            query = list(ErrorTable.query
                         .filter(ErrorTable.date <= date_a)
                         .filter(ErrorTable.date > date_b)
                         .order_by(ErrorTable.date.desc())
                         )

        for result in query:
            index = ret_data[result.platform]['x'].index(
                result.date.strftime('%d-%m-%Y'))
            ret_data[result.platform]['y'][index] += result.count
    for key in ret_data.keys():
        new = [y if y != 0 else None for y in ret_data[key]['y']]
        ret_data[key]['y'] = new
    return ret_data


def update_live_optimization_score(client_, platform):
    ret = {}
    date = datetime.date.today()
    if client_ is not None:
        results = len(list(Optimizations.query.filter_by(date=date)
                           .filter_by(client=client_)
                           .filter_by(platform=platform)
                           ))
        results_yest = len(list(Optimizations.query
                                .filter_by(date=date-datetime.timedelta(1))
                                .filter_by(client=client_)
                                .filter_by(platform=platform)
                                ))
    else:
        results = len(list(Optimizations.query.filter_by(date=date)
                           .filter_by(platform=platform)
                           ))
        results_yest = len(list(Optimizations.query.filter_by(
            date=date - datetime.timedelta(1)).filter_by(platform=platform)
                                ))
    if results_yest == 0:
        percents = 100
    else:
        percents = abs(100 * (results - results_yest) / results_yest)
    if percents >= 10:
        percents = round(percents)
    else:
        percents = round(percents, 1)
    ret['now'] = results
    ret['percents'] = f"{percents}%"
    if results - results_yest < 0:
        ret['dir'] = 'down'
    else:
        ret['dir'] = 'up'
    return ret


def update_live_error(client_, platform):
    ret = {}
    date = datetime.date.today()
    if client_ is not None:
        results = len(list(ErrorTable.query.filter_by(date=date)
                           .filter_by(client=client_)
                           .filter_by(platform=platform)
                           ))
        results_yest = len(list(ErrorTable.query
                                .filter_by(date=date - datetime.timedelta(1))
                                .filter_by(client=client_)
                                .filter_by(platform=platform)
                                ))
    else:
        results = len(list(ErrorTable.query.filter_by(date=date)
                           .filter_by(platform=platform)
                           ))
        results_yest = len(list(ErrorTable.query.filter_by(
            date=date - datetime.timedelta(1)).filter_by(platform=platform)
                                ))
    if results_yest == 0:
        percents = 100
    else:
        percents = abs(100 * (results - results_yest) / results_yest)
    if percents >= 10:
        percents = round(percents)
    else:
        percents = round(percents, 1)
    ret['now'] = results
    ret['percents'] = f"{percents}%"
    if results - results_yest < 0:
        ret['dir'] = 'down'
    else:
        ret['dir'] = 'up'
    return ret
