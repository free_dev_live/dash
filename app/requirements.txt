dash==0.28.1
dash-html-components==0.13.2
dash-core-components==0.30.2
dash-table-experiments==0.6.0
pandas==0.23.4
Flask-SQLAlchemy==2.3.2
psycopg2==2.7.5
