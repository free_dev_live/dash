import datetime
import urllib.parse

import controllers
import dash
import pandas as pd
from config import POSTGRES
from models import db, Optimizations, ErrorTable
from view import layout, line_chart, create_bar_chart, create_circle

external_stylesheets = [
    {
        'href': 'https://use.fontawesome.com/releases/v5.4.1/css/all.css',
        'rel': 'stylesheet',
        'integrity': """sha384-5sAR7xN1Nv6T6+dT2mht
                     zEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz""",
        'crossorigin': 'anonymous'
    }
]

SQLALCHEMY_DATABASE_URI = 'postgresql://%(user)s:' \
                          '@%(host)s:%(port)s/%(db)s' % POSTGRES

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.server.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
app.server.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db.init_app(app.server)
lay = layout(app)
app.layout = lay
app.head = [
    ("""
    <meta id="meta" name="viewport" content="width=device-width;
     initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">""")
]
days_limit = {
        'dbd': 30,
        'wbw': 90
    }


# Callback functions #


def create_plot_data(rows, value, type_chart, table_column, itter):
    plot_data = {'': [0, 0]}
    distinct_values_in_rows = controllers.combine(rows)
    if len(distinct_values_in_rows) != 0:
        date_set = (datetime.datetime.now() - datetime.timedelta(90 - value[0]),
                    datetime.datetime.now() - datetime.timedelta(90 - value[1]))
        plot_data = controllers.data_dict(distinct_values_in_rows,
                                          rows, controllers.DATA_BARS_RED,
                                          date_set,
                                          type_chart,
                                          table_column, itter, table_column,
                                          'Date')
    return plot_data


def update_bar_chart(rows, value, type_chart, table_column, itter, height,
                     width):
    plot_data = create_plot_data(rows, value, type_chart, table_column, itter)
    first_bar_chart_left = create_bar_chart(plot_data,
                                            'f_b_l_chart', 'stack',
                                            height, width)
    return first_bar_chart_left[0].figure


def dwn_data(rows, value, type_chart, table_column, itter):
    graph_data = create_plot_data(rows, value, type_chart,
                                  table_column, itter)
    dff = pd.DataFrame(graph_data)
    csv_string = dff.to_csv(index=False, encoding='utf-8')
    csv_string = "data:text/csv;charset=utf-8," + urllib.parse.quote(
        csv_string)
    return csv_string


def linear_charts_dwn(data):
    dff = pd.DataFrame(data)
    csv_string = dff.to_csv(index=False, encoding='utf-8')
    csv_string = "data:text/csv;charset=utf-8," + urllib.parse.quote(
        csv_string)
    return csv_string


# Line charts callbacks #
@app.callback(
    dash.dependencies.Output('l_f_l_chart', 'figure'),
    [dash.dependencies.Input('client', 'value'),
     dash.dependencies.Input('dpd_left_inner', 'value')])
def update_line_chart_left(client, days_format):
    data_ = controllers.optimizations_line_chart(app, client,
                                                 days_limit[days_format])
    chart = line_chart(data_, None, 300, 670)
    return chart.figure


@app.callback(
    dash.dependencies.Output('r_f_r_chart', 'figure'),
    [dash.dependencies.Input('client', 'value'),
     dash.dependencies.Input('dpd_right_inner', 'value')])
def update_line_chart_right(client, days_format):
    data_ = controllers.error_line_chart(app, client, days_limit[days_format])
    chart = line_chart(data_, None, 300, 670)
    return chart.figure

# Errors chart callbacks #


@app.callback(
    dash.dependencies.Output('platform_errors', 'figure'),
    [dash.dependencies.Input('data-table-1', 'rows'),
     dash.dependencies.Input('error_table_slider', 'value')]
)
def update_first_error_bar_chart(rows, value):
    return update_bar_chart(rows, value, "Error Type",
                            "Platform", 'count', 280, 570)


@app.callback(
    dash.dependencies.Output('market_errors', 'figure'),
    [dash.dependencies.Input('data-table-1', 'rows'),
     dash.dependencies.Input('error_table_slider', 'value')])
def update_second_error_bar_chart(rows, value):
    return update_bar_chart(rows, value, "Error Type", "Market", 'count',
                            280, 800)


@app.callback(
    dash.dependencies.Output('type_errors', 'figure'),
    [dash.dependencies.Input('data-table-1', 'rows'),
     dash.dependencies.Input('error_table_slider', 'value')])
def update_second_error_bar_chart(rows, value):
    return update_bar_chart(rows, value, "Error Type", "Error Type", 'count',
                            410, 1470)


@app.callback(
    dash.dependencies.Output('campaign_errors', 'figure'),
    [dash.dependencies.Input('data-table-1', 'rows'),
     dash.dependencies.Input('error_table_slider', 'value')])
def update_second_error_bar_chart(rows, value):
    return update_bar_chart(rows, value, "Error Type", "Compaign", 'count',
                            480, 1470)


# Optmization chart callbacks
@app.callback(
    dash.dependencies.Output('platform_optimization', 'figure'),
    [dash.dependencies.Input('data-table-2', 'rows'),
     dash.dependencies.Input('optimisation_table_slider', 'value')]
)
def update_first_error_bar_chart(rows, value):
    return update_bar_chart(rows, value, "Check Type", "Platform", 'Score',
                            280, 570)


@app.callback(
    dash.dependencies.Output('market_optimization', 'figure'),
    [dash.dependencies.Input('data-table-2', 'rows'),
     dash.dependencies.Input('optimisation_table_slider', 'value')])
def update_second_error_bar_chart(rows, value):
    return update_bar_chart(rows, value, "Check Type", "Market", 'Score',
                            280, 800)


@app.callback(
    dash.dependencies.Output('type_optimization', 'figure'),
    [dash.dependencies.Input('data-table-2', 'rows'),
     dash.dependencies.Input('optimisation_table_slider', 'value')])
def update_second_error_bar_chart(rows, value):
    return update_bar_chart(rows, value, "Check Type", "Check Type", 'Score',
                            410, 1470)


@app.callback(
    dash.dependencies.Output('campaign_optimization', 'figure'),
    [dash.dependencies.Input('data-table-2', 'rows'),
     dash.dependencies.Input('optimisation_table_slider', 'value')])
def update_second_error_bar_chart(rows, value):
    return update_bar_chart(rows, value, "Check Type", "Compaign", 'Score',
                            480, 1470)

# Create download callbacks for errors


@app.callback(
    dash.dependencies.Output('Errors Split By Platform', 'href'),
    [dash.dependencies.Input('data-table-1', 'rows'),
     dash.dependencies.Input('error_table_slider', 'value')]
)
def update_download_link(rows, value):
    return dwn_data(rows, value, "Error Type", "Platform", 'count')


@app.callback(
    dash.dependencies.Output('Errors Split By Market', 'href'),
    [dash.dependencies.Input('data-table-1', 'rows'),
     dash.dependencies.Input('error_table_slider', 'value')]
)
def update_download_link(rows, value):
    return dwn_data(rows, value, "Error Type", "Market", 'count')


@app.callback(
    dash.dependencies.Output('Errors Split by Type', 'href'),
    [dash.dependencies.Input('data-table-1', 'rows'),
     dash.dependencies.Input('error_table_slider', 'value')]
)
def update_download_link(rows, value):
    return dwn_data(rows, value, "Error Type", "Error Type", 'count')


@app.callback(
    dash.dependencies.Output('Errors Split by Campaign', 'href'),
    [dash.dependencies.Input('data-table-1', 'rows'),
     dash.dependencies.Input('error_table_slider', 'value')]
)
def update_download_link(rows, value):
    return dwn_data(rows, value, "Error Type", "Compaign", 'count')


# Create optimizations callbacks for files
@app.callback(
    dash.dependencies.Output('Optimizations Split By Platform', 'href'),
    [dash.dependencies.Input('data-table-2', 'rows'),
     dash.dependencies.Input('optimisation_table_slider', 'value')]
)
def update_download_link(rows, value):
    return dwn_data(rows, value, "Check Type", "Platform", 'Score')


@app.callback(
    dash.dependencies.Output('Optimizations Split By Market', 'href'),
    [dash.dependencies.Input('data-table-2', 'rows'),
     dash.dependencies.Input('optimisation_table_slider', 'value')]
)
def update_download_link(rows, value):
    return dwn_data(rows, value, "Check Type", "Market", 'Score')


@app.callback(
    dash.dependencies.Output('Optimizations Split by Type', 'href'),
    [dash.dependencies.Input('data-table-2', 'rows'),
     dash.dependencies.Input('optimisation_table_slider', 'value')]
)
def update_download_link(rows, value):
    return dwn_data(rows, value, "Check Type", "Check Type", 'Score')


@app.callback(
    dash.dependencies.Output('Optimizations Split by Campaign', 'href'),
    [dash.dependencies.Input('data-table-2', 'rows'),
     dash.dependencies.Input('optimisation_table_slider', 'value')]
)
def update_download_link(rows, value):
    return dwn_data(rows, value, "Check Type", "Compaign", 'Score')

# linear graphs download


@app.callback(
    dash.dependencies.Output('Optimization Score Over Time', 'href'),
    [dash.dependencies.Input('client', 'value'),
     dash.dependencies.Input('dpd_left_inner', 'value')])
def dwn_linear(client, value):
    data_ = controllers.optimizations_line_chart(app, client,
                                                 days_limit[value])
    return linear_charts_dwn(data_)


@app.callback(
    dash.dependencies.Output('Error Over Time', 'href'),
    [dash.dependencies.Input('client', 'value'),
     dash.dependencies.Input('dpd_right_inner', 'value')])
def dwn_linear(client, value):
    data_ = controllers.optimizations_line_chart(app, client,
                                                 days_limit[value])
    return linear_charts_dwn(data_)


# Circles callback
@app.callback(
    dash.dependencies.Output('live_opti_score', 'children'),
    [dash.dependencies.Input('client', 'value')]
)
def live_opti_score(value):
    ret = []
    platforms = ['AdWords', 'Facebook', 'Bing']
    data_for_circles = [controllers.update_live_optimization_score(value,
                                                                   platform)
                        for platform in platforms]
    for i in range(len(platforms)):
        platform_data = data_for_circles[i]
        circle = create_circle(platforms[i], 'lo', platform_data['now'],
                               platform_data['dir'], platform_data['percents'])
        ret.append(circle)
    return ret


@app.callback(
    dash.dependencies.Output('live_error_score', 'children'),
    [dash.dependencies.Input('client', 'value')]
)
def live_error_score(value):
    ret = []
    platforms = ['AdWords', 'Facebook', 'Bing', ]
    data_for_circles = [
        controllers.update_live_error(value, platform) for
        platform in platforms]
    for i in range(len(platforms)):
        platform_data = data_for_circles[i]
        circle = create_circle(platforms[i], 'le', platform_data['now'],
                               platform_data['dir'], platform_data['percents'])
        ret.append(circle)
    return ret

# Tables update callbacks

@app.callback(
    dash.dependencies.Output('data-table-1', 'rows'),
    [dash.dependencies.Input('error_table_slider', 'value'),
     dash.dependencies.Input('client', 'value')]
)
def update_error_table(value, client):
    date_b = datetime.datetime.now() - datetime.timedelta(90-value[0])
    date_a = datetime.datetime.now() - datetime.timedelta(90-value[1])
    if client is None:
        errors = list(
            ErrorTable.query.filter(ErrorTable.date <= date_a.strftime('%Y-%m-%d'))
            .filter(ErrorTable.date >= date_b.strftime('%Y-%m-%d')))
    else:
        errors = list(
            ErrorTable.query.filter(
                ErrorTable.date <= date_a.strftime('%Y-%m-%d'))
                .filter(ErrorTable.date >= date_b.strftime('%Y-%m-%d'))
                .filter(ErrorTable.client == client))
    data = controllers.error_table(errors)
    return data.to_dict('records')


@app.callback(
    dash.dependencies.Output('data-table-2', 'rows'),
    [dash.dependencies.Input('optimisation_table_slider', 'value'),
     dash.dependencies.Input('client', 'value')]
)
def update_error_table(value, client):
    date_b = datetime.datetime.now() - datetime.timedelta(90 - value[0])
    date_a = datetime.datetime.now() - datetime.timedelta(90 - value[1])
    if client is None:
        errors = list(
            Optimizations.query.filter(Optimizations.date <=
                                       date_a.strftime('%Y-%m-%d'))
            .filter(Optimizations.date >= date_b.strftime('%Y-%m-%d')))
    else:
        errors = list(
            Optimizations.query.filter(
                Optimizations.date <= date_a.strftime('%Y-%m-%d'))
                .filter(Optimizations.date >= date_b.strftime('%Y-%m-%d'))
                .filter(Optimizations.client == client))
    data = controllers.optimization_table(errors)
    return data.to_dict('records')


if __name__ == '__main__':
    app.run_server(host="0.0.0.0", debug=True)
