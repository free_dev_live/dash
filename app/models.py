from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Optimizations(db.Model):
    __tablename__ = 'optimizations'

    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime)
    market = db.Column(db.String(150))
    client = db.Column(db.String(150))
    account = db.Column(db.String(150))
    campaign = db.Column(db.String(150))
    adgroup = db.Column(db.String(150))
    error = db.Column(db.String(150))
    check_ = db.Column(db.String(150))
    platform = db.Column(db.String(150))
    type = db.Column(db.String(150))
    level = db.Column(db.String(150))
    entity_type = db.Column(db.String(150))
    item_details = db.Column(db.String(150))
    score = db.Column(db.Numeric)
    cost = db.Column(db.Integer)


class ErrorTable(db.Model):
    __tablename__ = 'errors'

    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime)
    market = db.Column(db.String(150))
    client = db.Column(db.String(150))
    account = db.Column(db.String(150))
    campaign = db.Column(db.String(150))
    adgroup = db.Column(db.String(150))
    entity_type = db.Column(db.String(150))
    item_type = db.Column(db.String(150))
    level = db.Column(db.String(150))
    item_details = db.Column(db.String(150))
    count = db.Column(db.Integer)
    platform = db.Column(db.String(150))
