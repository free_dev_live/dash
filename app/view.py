import dash_core_components as dcc
import dash_html_components as html
import dash_table_experiments as dt
import plotly.graph_objs as go
from controllers import section_data
import datetime

LOREM = "Lorem Ipsum is simply dummy text of the \
            printing and typesetting industry. Lorem Ipsum has been the \
            industry's standard dummy text ever since the 1500s, \
            when an unknown printer took a galley of type and scrambled \
            it to make a type specimen book. It has survived not only five \
            centuries "


def create_header(class_str, header_text):
    return html.Header(children=[
        html.Div(
            className=class_str,
            children=header_text
        )
    ])


def create_row_header(header, header_text, display=''):
    return html.Div(
        className=f"section-header {header}",
        children=[
            html.Div(
                className="inner",
                children=header_text
            ),
            html.A(children=[
                html.I(
                    className="fas fa-file-download"
                )
            ],
                id=header_text,
                download=f"{header_text}.csv",
                href="",
                target="_blank",
                style={
                'display': display
            })
        ]
    )


def column_graph(column_side, header_color, header_text,
                 height, width, children):
    column_side = f"{column_side}-colomn bordered"
    child = [create_row_header(header_color, header_text), *children]
    return html.Div(
        className=column_side,
        style={"height": f"{height}px", "width": f"{width}px"},
        children=child
    )


def create_circle(company, score_type, score='007', direction='up',
                  percents='87.3%'):
    return html.Div(
        className="inner-container",
        children=[
            html.Div(
                className=f"circle {company}",
                children=[
                    html.Div(
                        className="circle-number-main",
                        id=f"{score_type}_{company}_m_s",
                        children=score

                    ),
                    html.Div(
                        className="circle-inn-cont",
                        children=[
                            html.Div(
                                className=f"arrow-{direction}",
                                id=f"{score_type}_{company}_arr"
                            ),
                            html.Div(
                                className="circle-number-sub",
                                id=f"{score_type}_{company}_s_s",
                                children=percents
                            )
                        ]
                    )
                ]
            )
        ]
    )


def row_text(text):
    return html.Div(
        className="inner-container lr",
        children=[
            html.Div(
                className="text",
                children=text
            )
        ]
    )


def create_row(children, header_color, header_text, body_class=''):
    return html.Div(
        className="bordered",
        children=[
            html.Div(
                className="colomn",
                children=[
                    create_row_header(header_color, header_text, 'none'),
                    html.Div(
                        className=f"row centered {body_class} ",
                        children=children
                    )
                ]
            )
        ]
    )


def circles_in_row(id_, text, score_type):
    div_row = [create_circle(company, score_type)
               for company in
               ['AdWords', 'Facebook', 'Bing']]
    text_in = row_text(text)
    return [
        html.Div(
            id=id_,
            children=div_row
        ),
        text_in
    ]


def create_table(data_, _id, slice_):
    columns = data_.columns
    if slice_:
        columns = data_.columns[:len(data_.columns)-1]
    ret = dt.DataTable(
        rows=data_.to_dict('records'),
        columns=columns,
        filterable=True,
        id=_id
    )
    return ret


def line_chart(_data, _id, height, width):

    colors = {
        'Bing': ('rgb(143, 87, 220)'),
        'Facebook': ('rgb(71, 165, 227)'),
        'AdWords': ('rgb(21, 187, 173)'),
    }
    return dcc.Graph(
        id=_id,
        config={'displayModeBar': True},
        figure={
            'data': [
                go.Scatter(
                    x=_data[key]['x'],
                    y=_data[key]['y'],
                    mode="lines+markers",
                    line={'shape': 'spline', 'color': colors[key]},
                    marker=dict(
                        size=10,
                    ),
                    connectgaps=True
                )
                for key in _data.keys()
            ],
            'layout': go.Layout(
                height=height,
                width=width,
                showlegend=False,
                xaxis=dict(
                    autorange=True,
                    tickangle=90,
                    rangemode='tozero'
                ),
                yaxis=dict(autorange=True, rangemode='nonnegative'),
                margin=go.layout.Margin(
                    l=60,
                    r=0,
                    b=50,
                    t=10,
                    pad=0
                )

            )
        },
    )


def create_line_chart(_data, _id, column_side, height, width):
    chart = line_chart(_data, _id, height, width)
    return [html.Div(
        className="centered",
        children=[
            dcc.Dropdown(
                id=f"dpd_{column_side}_inner",
                options=[
                    {'label': "Week by Week", 'value': 'wbw'},
                    {'label': 'Day by Day', 'value': 'dbd'},
                ],
                value='dbd'
            ),
            html.Div(
                className='AdWords logo',
                children="AdWorks"
            ),
            html.Div(
                className='Facebook logo',
                children="Facebook"
            ),
            html.Div(
                className='Bing logo',
                children="Bing"
            )
        ]
    ),
        chart
    ]


def create_bar_chart(_data, _id, mode, height, width):
    colors = [('#f44236'), ('#f5b400')]
    slider_id = f"{_id}_slider"
    return [
        dcc.Graph(
            id=_id,
            figure={
                'data': [
                    go.Bar(
                        x=list(_data.keys()),
                        y=[_data[key][i] for key in _data.keys()],
                        marker=dict(
                            color=colors[i]
                        )
                    ) for i in range(2)
                ],
                'layout': go.Layout(
                    height=height,
                    width=width,
                    showlegend=False,
                    barmode=mode,
                    margin=go.layout.Margin(
                        l=60,
                        r=0,
                        b=60,
                        t=30,
                        pad=0
                    ),
                    xaxis=dict(showline=True, rangemode='tozero'),
                    yaxis=dict(showline=False, rangemode='nonnegative'),
                )
            }
        )]


def section(data_):
    data_client = data_['client']
    right_line_chart = create_line_chart({'AdWords': {'x': [], 'y': []}},
                                         'r_f_r_chart',
                                         'right', 300, 670)
    left_line_chart = create_line_chart({'AdWords': {'x': [], 'y': []}},
                                        'l_f_l_chart',
                                        'left', 300, 670)
    first_bar_chart_left = create_bar_chart(data_['empty_bar_chart'],
                                            'platform_errors', 'stack',
                                            280, 570)
    first_bar_chart_right = create_bar_chart(data_['empty_bar_chart'],
                                             'market_errors', 'stack',
                                             280, 800)
    first_row_bar_graph = create_bar_chart(data_['empty_bar_chart'],
                                           'type_errors', 'stack',
                                           410, 1470)
    first_row_bar_graph_ = create_bar_chart(data_['empty_bar_chart'],
                                            'campaign_errors', 'stack',
                                            480, 1470)
    #
    #
    second_bar_chart_left = create_bar_chart(data_['empty_bar_chart'],
                                             'platform_optimization', 'stack',
                                             280, 570)
    second_bar_chart_right = create_bar_chart(data_['empty_bar_chart'],
                                              'market_optimization', 'stack',
                                              280, 800)
    second_row_bar_graph = create_bar_chart(data_['empty_bar_chart'],
                                            'type_optimization', 'stack',
                                            410, 1470)
    second_row_bar_graph_ = create_bar_chart(data_['empty_bar_chart'],
                                             'campaign_optimization', 'stack',
                                             480, 1470)
    return html.Section(children=[
        html.Div(
            className="container",
            children=[
                html.Div(
                    className="left-colomn",
                    children=[
                        html.Div(
                            className="start",
                            children=[
                                dcc.Dropdown(
                                    id="client",
                                    placeholder="Select Client",
                                    options=[
                                        {'label': f'{client}',
                                         'value': f'{client}'}
                                        for client in data_client
                                    ]
                                )
                            ]
                        )
                    ]
                ),
                html.Div(
                    className="right-colomn",
                    children=[
                        html.Div(className="end", id="graph_tools_f")
                    ]
                ),
            ]
        ),
        html.Div(
            className="container",
            children=[
                column_graph('left', 'blue',
                             "Optimization Score Over Time",
                             530, 745,
                             left_line_chart),
                column_graph('right', 'red',
                             "Error Over Time",
                             530, 745,
                             right_line_chart)
            ]
        ),
        html.Div(
            className="container",
            children=[create_row(circles_in_row('live_opti_score',
                                                LOREM, 'lo'), 'blue',
                                 "Live Optimization Score")]
        ),
        html.Div(
            className="container",
            children=[create_row(circles_in_row('live_error_score', LOREM,
                                                'le'), 'red',
                                 "Live Error Score")]
        ),
        dcc.RangeSlider(
            id='error_table_slider',
            min=0,
            max=90,
            value=[0, 90],
            marks={i: (datetime.datetime.now() -
                       datetime.timedelta(90 - i))
                .strftime("%Y-%m-%d") for i in range(0, 91, 5)}
        ),
        html.Div(
            className="container",
            id='red',
            children=[
               create_table(data_['table_1'], 'data-table-1', True)
            ],
        ),
        html.Div(
            className="container",
            children=[
                column_graph('left', 'red', 'Errors Split By Platform',
                             360, 630,
                             first_bar_chart_left),
                column_graph('right', 'red', 'Errors Split By Market',
                             360, 860,
                             first_bar_chart_right)
            ]
        ),
        html.Div(
            className="container",
            children=[
                column_graph('left', 'red', 'Errors Split by Type',
                             490, 1530,
                             first_row_bar_graph
                             )
            ]
        ),
        html.Div(
            className="container",
            children=[
                column_graph('left', 'red', 'Errors Split by Campaign', 560,
                             1530,
                             first_row_bar_graph_
                             )
            ]
        ),
        dcc.RangeSlider(
            id='optimisation_table_slider',
            min=0,
            max=90,
            value=[0, 90],
            marks={i: (datetime.datetime.now() -
                       datetime.timedelta(90-i))
                   .strftime("%Y-%m-%d") for i in range(0, 91, 5)}
        ),
        html.Div(
            className="container",
            id='blue',
            children=[
                create_table(data_['table_2'], 'data-table-2', False)
            ]
        ),
        html.Div(
            className="container",
            children=[
                column_graph('left', 'blue', 'Optimizations Split By Platform',
                             360, 630,
                             second_bar_chart_left),
                column_graph('right', 'blue', 'Optimizations Split By Market',
                             360, 860,
                             second_bar_chart_right)
            ]
        ),
        html.Div(
            className="container",
            children=[
                column_graph('left', 'blue', 'Optimizations Split by Type',
                             490, 1530,
                             second_row_bar_graph
                             )
            ]
        ),
        html.Div(
            className="container",
            children=[
                column_graph('left', 'blue', 'Optimizations Split by Campaign',
                             560, 1530,
                             second_row_bar_graph_
                             )
            ]
        )

    ]
    )


def layout(app):
    data_sec = section_data(app)
    lay = html.Div(
        id='layout',
        children=[
            create_header("header", "Best Practise Check"),
            section(data_sec)
    ])
    return lay
